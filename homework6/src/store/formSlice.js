import { createSlice } from "@reduxjs/toolkit";


const formSlice = createSlice({
    name: "form",
    initialState: {
        formData: {
            firstName: "",
            lastName: "",
            age: "",
            country: "",
            streetAddress: "",
            city: "",
            postalCode: "",
            phone: ""
          
          }
    },
    reducers: {
        actionUpDateForm: (state, {payload})=>{
            state.formData = {...payload}
        }
    }

})
export const {actionUpDateForm} = formSlice.actions


export default formSlice.reducer