export const selectAllProducts = (state) => state.product.allProducts
export const selectIsModals = (state) => state.app.isModal
export const selectAddBuy = (state) => state.app.buyArr
export const selectAddFav = (state) => state.app.favArr
export const selectMacProduct = (state) => state.product.macProduct
export const selectIpadProduct = (state)=> state.product.ipadProduct
export const selectIphoneProduct = (state)=> state.product.iphoneProduct
export const selectFromData = (state) => state.form.formData
export const selectWatchProduct = (state)=> state.product.watchProduct
export const selectAirPodsProduct = (state)=>state.product.airPodsProduct
