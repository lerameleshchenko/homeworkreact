import {configureStore} from '@reduxjs/toolkit'

import appReducer from './appSlice'
import productReducer from './productSlice'
import formReducer from './formSlice'
export default configureStore({
    reducer: {
        app: appReducer,
        product: productReducer,
        form :formReducer
    },

})