import { createSlice } from "@reduxjs/toolkit";
import { sendRequest } from "../helpers/sendRequest.js";

const productSlice = createSlice({
    name: "product",
    initialState: {
        allProducts:[],
        macProduct: [], 
        ipadProduct: [],
        iphoneProduct:[],
        watchProduct: [],
        airPodsProduct:[]
    },
    reducers: {
        actionAddAllProducts: (state, {payload})=>{
            state.allProducts = payload
        },
        acrionProductMac:(state) =>{
            state.macProduct = state.allProducts.filter((product) => product.nameProduct === "mac");
        },
        actionProductIpad: (state)=>{
            state.ipadProduct = state.allProducts.filter((product) => product.nameProduct === "ipad");
        },
        actionProductiPhone:(state)=>{
            state.iphoneProduct = state.allProducts.filter((product) => product.nameProduct === "iphone");
        },
        actionProductWatch:(state)=>{
            state.watchProduct = state.allProducts.filter((product) => product.nameProduct === "watch");
        },
        actionProductAirPods:(state)=>{
            state.airPodsProduct = state.allProducts.filter((product) => product.nameProduct === "airPods");
        }
    }

})
export const {actionAddAllProducts,
    acrionProductMac,
    actionProductIpad,
    actionProductiPhone,
    actionProductWatch,
    actionProductAirPods
} = productSlice.actions

export const actionFetchAllProducts = () => (dispatch) => {
    return sendRequest('data.json')
    .then((data) =>{
        dispatch(actionAddAllProducts(data));
        dispatch(acrionProductMac())
        dispatch(actionProductIpad())
        dispatch(actionProductiPhone())
        dispatch(actionProductWatch())
        dispatch(actionProductAirPods())
    })
    
}




export default productSlice.reducer