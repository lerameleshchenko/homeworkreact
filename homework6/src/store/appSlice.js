import { createSlice } from "@reduxjs/toolkit";

const appSlice = createSlice({
    name:"app",
    initialState:{
	isModal: false,
    buyArr: JSON.parse(localStorage.getItem('cart')) || [],
    favArr: JSON.parse(localStorage.getItem('favoriteProducts')) || []
	},

    reducers:{
        actionIsModals: (state)=>{
            state.isModal = !state.isModal
        },
        actionProductInBuy: (state, {payload}) =>{
            const existingProductIndex = state.buyArr.findIndex((product) => product.article === payload.article);
        if (existingProductIndex !== -1) {
          state.buyArr[existingProductIndex].quantity += 1;
        } else {
          state.buyArr = [...state.buyArr, { ...payload, quantity: 1 }];
        }
        localStorage.setItem('cart', JSON.stringify(state.buyArr));
        },
        actionProductInBuyDelete:(state, {payload})=>{
            const existingProductIndex = state.buyArr.findIndex((product) => product.article === payload);
            if (existingProductIndex !== -1) {
              if (state.buyArr[existingProductIndex].quantity > 1) {
                state.buyArr[existingProductIndex].quantity -= 1;
              } else {
                state.buyArr.splice(existingProductIndex, 1);
              }
            }
            localStorage.setItem('cart', JSON.stringify(state.buyArr));
        },
        actionProductInFav:(state, {payload})=>{
            state.favArr = [...state.favArr, payload]
        },
        actionProductInFavDelete:(state, {payload})=>{
            state.favArr = state.favArr.filter((product) => product.article !== payload);
        },
        actionCleanCart:(state)=>{
            state.buyArr = []
            localStorage.removeItem('cart')
        }  
      }

})

export const {actionIsModals,
    actionProductInBuy,
    actionProductInBuyDelete,
    actionProductInFav,
    actionProductInFavDelete,
    actionCleanCart
} = appSlice.actions



export default appSlice.reducer
