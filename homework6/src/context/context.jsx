import {createContext, useState} from "react";


export const CardContext = createContext()

const CardProvider = ({children}) => {
	const [changeCard, useChangeCard] = useState(false)

function change(){
    useChangeCard(!changeCard)
}

	return (
		<CardContext.Provider value={{change, changeCard}} >
			{children}
		</CardContext.Provider>
	)
}

export default CardProvider
