import Logo from "../icons/logo.svg?react"
import Favorite from "../icons/favorite.svg?react"
import Buy from "../icons/bay.svg?react"
import PropTypes from "prop-types"
import { useState } from "react"
import "./Header.scss"
import {Link} from "react-router-dom"
import { useSelector } from "react-redux"
import { selectAddBuy,selectAddFav } from "../../store/selectors.js"
function Header(){
    const cart = useSelector(selectAddBuy)
    const favoriteProducts = useSelector(selectAddFav)
    const [burgerMenu, useBurgerMenu] = useState(false)
    
    function changeMenu(){
        useBurgerMenu(!burgerMenu)
    }

    return(
        <>
          <header className="headeer">
           <Link to="/"><Logo/></Link> 
           <ul className="navMenu">
            <li><Link to="/">All</Link></li>
            <li><Link to="Mac">Mac</Link></li>
            <li><Link to="iPad">iPad</Link></li>
           <li><Link to="iphone">iPhone</Link></li>
            <li><Link to="watch">Watch</Link></li>
            <li><Link to="airPods">AirPods</Link></li>
           </ul>
           <div className="conteinerBurger" onClick={() => changeMenu()}>
           <div className="burger-menu"></div>
           </div>
            <div className="container-for-svg">
           <Link to="fvPage"><Favorite/></Link> 
            <p className="numF">{favoriteProducts.length}</p>
            <Link to="BuyPage"><Buy/></Link>
            <p className="numB">{cart.length}</p>
            </div>
           
        </header>
       {burgerMenu && (
 <ul className="navMenu2">
 <Link onClick={()=> changeMenu()} to="/"><li className="navItem">All</li></Link>
 <Link  onClick={()=> changeMenu()}to="Mac"><li className="navItem">Mac</li></Link>
 <Link onClick={()=> changeMenu()}to="iPad"><li className="navItem">iPad</li></Link>
<Link onClick={()=> changeMenu()}to="iphone"><li className="navItem">iPhone</li></Link>
<Link onClick={()=> changeMenu()} to="watch"><li className="navItem">Watch</li></Link>
<Link onClick={()=> changeMenu()}to="airPods"><li className="navItem">AirPods</li></Link>
</ul>
       )}
        </>
      
    )
}

Header.propTypes = {
    num1: PropTypes.any,
    num2: PropTypes.any
}

export default Header