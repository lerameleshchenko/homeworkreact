import Footer from "./Footer";
import {render, screen} from "@testing-library/react"
import React from "react";

describe('test footer', ()=>{
    test('Snapshot', ()=>{
        const footer = render(<Footer/>)
        expect(footer).toMatchSnapshot()
    })
    test('some text', ()=>{
        render(<Footer/>)
        expect(screen.getByText(/Copyright © 2023 Euphoria Folks Pvt Ltd. All rights reserved/))
    })
})