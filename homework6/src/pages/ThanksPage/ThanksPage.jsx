import { Link } from "react-router-dom"
import "./ThanksPage.scss"
function ThanksPage(){
    return(
        <div className="thanksContainer">
            <p className="thanksTekst">Thank you for your purchase!</p>
            <Link className="linkHome" to="/">Home Page</Link> 
        </div>
    )
}

export default ThanksPage