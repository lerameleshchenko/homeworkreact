import "./FvPage.scss"
import ProductCard from "../../components/Product/ProductCard";
import ProductItem from "../../components/Product/ProductItem";
import ModalImage from "../../components/ModalImage/ModalImage";

import {useDispatch, useSelector} from "react-redux"
import {useState} from "react"

import { actionProductInFavDelete ,actionIsModals,actionProductInBuy} from "../../store/appSlice.js"; 
import { selectAddFav,selectIsModals, selectAddBuy} from "../../store/selectors";

function FvPage() { 
  const addBuy = useSelector(selectAddBuy)
  const favoriteArr = useSelector(selectAddFav)
  const dispatch = useDispatch()
  const [selectedProduct, setSelectedProduct] = useState(null);
  const addCard = useSelector(selectIsModals)

  
   function hendlModal(){
      dispatch(actionIsModals())
   }
  
  function add(index){
   hendlModal()
   setSelectedProduct(favoriteArr[index]);
  }
  
  function addPlusNum() {
  dispatch(actionProductInBuy(selectedProduct))
  
  const currentCart = JSON.parse(localStorage.getItem('cart')) || [];
  currentCart.push(selectedProduct);
  localStorage.setItem('cart', JSON.stringify(currentCart));
   add();
  }

 
  console.log(favoriteArr)
 
   function handleDeleteFavorite(article) {
dispatch(actionProductInFavDelete(article))
const currentfav = JSON.parse(localStorage.getItem('favoriteProducts')) || [];
    const updatedCart = currentfav.filter((product) => product.article !== article);
    localStorage.setItem('favoriteProducts', JSON.stringify(updatedCart));
  };


    return(
      <>
        <ProductCard >
               {favoriteArr && favoriteArr.length > 0 ? (
        favoriteArr.map(({ name, article, color, img, price }, index) => (
          <ProductItem
            key={index}
            name={name}
            color={color}
            price={price}
            article={article}
            img={img}
            classNameFav=" favorite"
            clickFavorite={() => handleDeleteFavorite(article)}
            addToCard={() => add(index)}
            addcardText={` ${addBuy.some((item) => item.article === article) ? `added ✔` : ' add card'}`}
          />
        ))
      ) : (
        <p className="noProduct">There are no products added</p>
      )}
       
          </ProductCard>  

          {addCard && selectedProduct &&(
            <ModalImage
            img={selectedProduct.img}
            title={`Add Product ${selectedProduct.name}?`}
            desc="Description for you product"
            closeModal={add}
            handleBtn={add}
            handleOk={addPlusNum}
            isOpen={add}
          />
          )}
      </>
         

     
    )
}

export default FvPage