import * as Yup from "yup"


export const validationSchema = Yup.object({
    firstName: Yup.string()
    .matches(/[а-яА-ЯёЁa-zA-Z]$/, 'Не може мати числові значення')
    .required('Обовʼязкове поле'),

    lastName: Yup.string()
    .matches(/[а-яА-ЯёЁa-zA-Z]$/, 'Не може мати числові значення')
    .required('Обовʼязкове поле'),

    age: Yup.string()
    .matches(/^[0-9]+$/, 'Має містити тільки числові значення')
    .required('Обовʼязкове поле'),

    country: Yup.string()
    .matches(/^[a-zA-Zа-яА-ЯєЄїЇіІйЙ'’\s]+$/, 'Неприпустимі символи. Введіть правильну назву країни')
    .min(2, 'Мінімум 2 символи')
    .max(50, 'Максимум 50 символів')
    .required('Обовʼязкове поле'),

    streetAddress: Yup.string()
    .required('Обовʼязкове поле'),

    city: Yup.string()
    .required('Обовʼязкове поле'),

    postalCode:Yup.string()
    .required('Обовʼязкове поле'),


    phone:Yup.string()
    .required('Обовʼязкове поле'),
})