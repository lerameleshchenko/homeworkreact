
import ProductItem from "../../components/Product/ProductItem";
import ProductItemTable from "../../components/Product/ProductItemTable";
import ProductCard from "../../components/Product/ProductCard";
import ProductCardTable from "../../components/Product/ProductCardTable";
import ModalImage from "../../components/ModalImage/ModalImage";
import {useState, useEffect, useContext} from "react"
import {useDispatch, useSelector} from "react-redux"

import { CardContext } from "../../context/context";
import Button from "../../components/Button/Button";
import { actionFetchAllProducts } from "../../store/productSlice";
import {actionIsModals, actionProductInFav, actionProductInFavDelete,actionProductInBuy} from "../../store/appSlice.js";
import { selectIsModals,selectAddFav,selectAddBuy,selectIpadProduct  } from "../../store/selectors";


function IpadPage(){
     //add fav
const ipadproduct = useSelector(selectIpadProduct);
const [isFavorite, setIsFavorite] = useState(Array(ipadproduct.length).fill(false));
  const favoriteProducts = useSelector(selectAddFav);

const dispatch = useDispatch()

useEffect(()=>{

dispatch(actionFetchAllProducts())

},[])

useEffect(() => {
    const newIsFavorite = Array(ipadproduct.length).fill(false);
    favoriteProducts.forEach((favProduct) => {
      const index = ipadproduct.findIndex((product) => product.article === favProduct.article);
      if (index !== -1) {
        newIsFavorite[index] = true;
      }
    });
    setIsFavorite(newIsFavorite);
  }, [favoriteProducts, ipadproduct]);

  function changeFavorite(index) { // index кожної карточки
    const newIsFavorite = [...isFavorite];
    newIsFavorite[index] = !newIsFavorite[index];
    setIsFavorite(newIsFavorite);

    const productFav = ipadproduct[index];
    if (newIsFavorite[index]) {
      dispatch(actionProductInFav(productFav));
      const currentCartFaxv = JSON.parse(localStorage.getItem('favoriteProducts')) || [];
      currentCartFaxv.push(productFav);
      localStorage.setItem('favoriteProducts', JSON.stringify(currentCartFaxv));
    } else {
      dispatch(actionProductInFavDelete(productFav.article));
      const currentCartDelFav = JSON.parse(localStorage.getItem('favoriteProducts')) || [];
      const updatedCartfav = currentCartDelFav.filter((product) => product.article !== productFav.article);
      localStorage.setItem('favoriteProducts', JSON.stringify(updatedCartfav));
    }
    }

//addBuy
    const addedToCart = useSelector(selectAddBuy);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const addCard = useSelector(selectIsModals)

    
     function hendlModal(){
        dispatch(actionIsModals())
     }
    
    function add(index){
     hendlModal()
     setSelectedProduct(ipadproduct[index]);
    }
    
    function addPlusNum() {
    dispatch(actionProductInBuy(selectedProduct))
     add();
    }

    const {change, changeCard} = useContext(CardContext)


    return(
        <>
         <div className="container-change">
        <Button onClick={change} classNames="change-style">change style</Button>
        </div>
         <ProductCard>
        {ipadproduct.map((product,index)=>(
       (changeCard === false) && (
        <ProductItem
          key={index}
          quantity={product.quantity}
          name={product.name}
          color={product.color}
          price={product.price}
          article={product.article}
          img={product.img}
          clickFavorite={() => changeFavorite(index)}
          classNameFav={` ${isFavorite[index] ? "favorite" : ""}`}
          addToCard={() => add(index)}
          addcardText={` ${addedToCart.some((item) => item.article === product.article) ? `add + ` : ' add card'}`}
        />
      )
        ))}
       </ProductCard>

       {(changeCard === true) && (
  <ProductCardTable>
    {ipadproduct.map((product, index) => (
      <ProductItemTable
        key={index}
        quantity={product.quantity}
        name={product.name}
        color={product.color}
        price={product.price}
        article={product.article}
        img={product.img}
        clickFavorite={() => changeFavorite(index)}
        classNameFav={` ${isFavorite[index] ? "favorite" : ""}`}
        addToCard={() => add(index)}
        addcardText={` ${addedToCart.some((item) => item.article === product.article) ? `add + ` : ' add card'}`}
      />
    ))}
  </ProductCardTable>
)}         
 {addCard && selectedProduct &&(
          <ModalImage
          img={selectedProduct.img}
          title={`Add Product ${selectedProduct.name}?`}
          desc="Description for you product"
          closeModal={add}
          handleBtn={add}
          handleOk={addPlusNum}
          isOpen={add}
        />
        )}
        </>
      
    )
}

export default IpadPage