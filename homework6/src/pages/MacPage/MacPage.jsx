import "./MacPage.scss"
import React from "react";


import ProductItem from "../../components/Product/ProductItem";
import ProductItemTable from "../../components/Product/ProductItemTable";
import ProductCard from "../../components/Product/ProductCard";
import ProductCardTable from "../../components/Product/ProductCardTable";
import ModalImage from "../../components/ModalImage/ModalImage";

import {useState, useEffect, useContext} from "react"
import {useDispatch, useSelector} from "react-redux"

import { CardContext } from "../../context/context";
import Button from "../../components/Button/Button";
import { actionFetchAllProducts } from "../../store/productSlice.js";
import {actionIsModals, actionProductInFav, actionProductInFavDelete,actionProductInBuy} from "../../store/appSlice.js"
import { selectIsModals,selectMacProduct,selectAddFav,selectAddBuy  } from "../../store/selectors";

function MacPage(){
  const addedToCart = useSelector(selectAddBuy);
  const macProducts = useSelector(selectMacProduct)
  const dispatch = useDispatch()
 const [isFavorite, setIsFavorite] = useState(Array(macProducts.length).fill(false));
const favoriteProducts = useSelector(selectAddFav)

useEffect(() => {
  dispatch(actionFetchAllProducts())
 
    
}, []);


useEffect(() => {
  const newIsFavorite = Array(macProducts.length).fill(false);
  favoriteProducts.forEach((favProduct) => {
    const index = macProducts.findIndex((product) => product.article === favProduct.article);
    if (index !== -1) {
      newIsFavorite[index] = true;
    }
  });
  setIsFavorite(newIsFavorite);
}, [favoriteProducts, macProducts]);


  function changeFavorite(index) { // index кожної карточки
      const newIsFavorite = [...isFavorite];
      newIsFavorite[index] = !newIsFavorite[index];
      setIsFavorite(newIsFavorite);
  
      const productFav = macProducts[index];
      if (newIsFavorite[index]) {
        dispatch(actionProductInFav(productFav));
        const currentCartFaxv = JSON.parse(localStorage.getItem('favoriteProducts')) || [];
        currentCartFaxv.push(productFav);
        localStorage.setItem('favoriteProducts', JSON.stringify(currentCartFaxv));
      } else {
        dispatch(actionProductInFavDelete(productFav.article));
        const currentCartDelFav = JSON.parse(localStorage.getItem('favoriteProducts')) || [];
        const updatedCartfav = currentCartDelFav.filter((product) => product.article !== productFav.article);
        localStorage.setItem('favoriteProducts', JSON.stringify(updatedCartfav));
      }
      }

//addBuy
      const [selectedProduct, setSelectedProduct] = useState(null);
      const addCard = useSelector(selectIsModals)
  
      
       function hendlModal(){
          dispatch(actionIsModals())
       }
      
      function add(index){
       hendlModal()
       setSelectedProduct(macProducts[index]);
      }
      
      function addPlusNum() {
      dispatch(actionProductInBuy(selectedProduct))
       add();
      }
  const {change, changeCard} = useContext(CardContext)

  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const [isVideoVisible, setIsVideoVisible] = useState(true);

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    setIsVideoVisible(windowWidth >= 768);
  }, [windowWidth]);

    return(
      <>
      <div className="titleMac">
        <h1 className="MACtitle">MAC</h1>
        <p className="titleDream">You dream it,<br />
          Mac can do it.</p>
      </div>
     
      {isVideoVisible && (
        <video className="video" loop autoPlay muted controls>
          <source src="./video/macIphone.mp4" type="video/mp4" />
        </video>
      )}
        <div className="container-change">
        <Button onClick={change} classNames="change-style">change style</Button>
        </div>
        <ProductCard>
  {macProducts.map((product, index) => (
    (changeCard === false) && (
      <ProductItem
        key={index}
        quantity={product.quantity}
        name={product.name}
        color={product.color}
        price={product.price}
        article={product.article}
        img={product.img}
        clickFavorite={() => changeFavorite(index)}
        classNameFav={` ${isFavorite[index] ? "favorite" : ""}`}
        addToCard={() => add(index)}
        addcardText={` ${addedToCart.some((item) => item.article === product.article) ? `add + ` : ' add card'}`}
      />
    )
  ))}
</ProductCard>

{(changeCard === true) && (
  <ProductCardTable>
    {macProducts.map((product, index) => (
      <ProductItemTable
        key={index}
        quantity={product.quantity}
        name={product.name}
        color={product.color}
        price={product.price}
        article={product.article}
        img={product.img}
        clickFavorite={() => changeFavorite(index)}
        classNameFav={` ${isFavorite[index] ? "favorite" : ""}`}
        addToCard={() => add(index)}
        addcardText={` ${addedToCart.some((item) => item.article === product.article) ? `add + ` : ' add card'}`}
      />
    ))}
  </ProductCardTable>
)}

           
           
 {addCard && selectedProduct &&(
          <ModalImage
          img={selectedProduct.img}
          title={`Add Product ${selectedProduct.name}?`}
          desc="Description for you product"
          closeModal={add}
          handleBtn={add}
          handleOk={addPlusNum}
          isOpen={add}
        />
        )}
        
      </>
        
     
  )

        
      
    
}

export default MacPage