import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import store from "./store"
import {BrowserRouter} from "react-router-dom"
import { Provider } from "react-redux"
import CardProvider from './context/context.jsx'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
      <CardProvider>
      <App />
      </CardProvider>
    </BrowserRouter>
    </Provider>
  </React.StrictMode>,
)
