import React from "react";

import FavoriteTwo from "../../composition/icons/favorite2.svg?react"

import Button from "../Button/Button"

import PropTypes from 'prop-types'

import "./ProductTable.scss"

function ProductItemTable({ name, price, img, article, quantity, color,clickFavorite,classNameFav,addcardText, addToCard, closeIcon,clickIconDelete}){
    return(
        <li className="itemTableCard">
        <img className="all-img" src={img} alt={name} />
        <div className="name">
        <h3 className="nameProductTable">{name}</h3>
        <p> {color}</p>
        <p className="priceTable">€ { price }</p>
        <p>{quantity}</p>
        </div>
       <div className="add">
       {closeIcon && (<Button classNames="btn-add" className={closeIcon} onClick={clickIconDelete}>delete</Button>)}
       {classNameFav &&( <FavoriteTwo onClick={clickFavorite} className={`favoriteDefault ${classNameFav}`}/> )}
       <Button onClick={addToCard} classNames="btn-add"><p className="textAddCard">{addcardText}</p></Button>
       </div>
         </li>
    )
}


ProductItemTable.defaultProps = {
    clickFavorite: () => {},
    addToCard: ()=>{}
  }

  ProductItemTable.propTypes = {
    name: PropTypes.string,
    img: PropTypes.string,
    price: PropTypes.number,
    article: PropTypes.number,
    color: PropTypes.string,
    classNameFav: PropTypes.any,
    clickFavorite: PropTypes.func,
    addToCard: PropTypes.func
}


export default ProductItemTable