import React from "react";

import "./ProductTable.scss"
import PropTypes from 'prop-types'


function ProductCardTable({children}){
    return (
        <>
          <ul className="productTable-wrapper">
       {children}
          </ul>
        </>
    )
}


ProductCardTable.propTypes = {
    children: PropTypes.any
}

export default ProductCardTable