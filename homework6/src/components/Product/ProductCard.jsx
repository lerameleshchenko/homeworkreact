import React from "react";

import "./Product.scss"
import PropTypes from 'prop-types'


function ProductCard({children}){
    return (
        <>
          <ul className="product-wrapper">
       {children}
          </ul>
        </>
    )
}


ProductCard.propTypes = {
    children: PropTypes.any
}

export default ProductCard