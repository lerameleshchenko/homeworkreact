import React from "react";
import { render, screen,fireEvent } from "@testing-library/react";
import ModalBase from "./ModalBase";
const hendelClick = jest.fn()
describe('test modal', () => {
    test('is modal', () => {
        render(<ModalBase title='test'/>);
        expect(screen.getByText("test")).toBeInTheDocument();
    });
    test('closeModal', ()=>{
 const {container} =  render(<ModalBase isOpen={hendelClick}/>)
  const buttonClose = container.firstChild
  fireEvent.click(buttonClose)
    expect(hendelClick).toHaveBeenCalled() 
    })
 
});
