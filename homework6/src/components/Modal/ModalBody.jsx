import React from "react";

import PropTypes from 'prop-types'
function ModalBody({children}){
    return(
        <div className="modal-content">{children}</div>
    )
}
 ModalBody.propTypes = {
    children: PropTypes.any
 }

export default ModalBody