import React from "react";
import PropTypes from 'prop-types'
function Modal({ children, classNames }) {
    const stopPropagation = (e) => {
        e.stopPropagation();
    };

    return (
        <div className={classNames} onClick={stopPropagation}>
            <div className="box-modal">
                {children}
            </div>
        </div>
    );
}

Modal.propTypes = {
    children: PropTypes.any,
    classNames: PropTypes.string
}

export default Modal;