import React from "react"
import Button from "./Button"
import { render, screen, fireEvent } from "@testing-library/react"
const hendelClick = jest.fn()
describe('test Button', ()=>{
test('isButton', ()=>{
    render(<Button>text</Button>)
    expect(screen.getByText("text")).toBeInTheDocument()

})

test("isClassName", ()=>{
    render(<Button classNames="btn-primary">text</Button>)
    expect(screen.getByText("text")).toHaveClass("btn-primary")
})

test("isType", ()=>{
    render(<Button>text</Button>)
    expect(screen.getByText("text")).toHaveAttribute('type', 'button') 
})

test('chenge', ()=>{
    render(<Button type={'submit'}>text</Button>)
    expect(screen.getByText("text")).toHaveAttribute('type', 'submit') 
})
test('onClick', ()=>{

  const {container} =  render(<Button onClick={hendelClick}>text</Button>)
  const button = container.firstChild
  fireEvent.click(button)
    expect(hendelClick).toHaveBeenCalled() 
})

})