import React from "react";

import './button.scss';
import PropTypes from 'prop-types'
function Button(props){
    const{type="button", classNames = "", onClick, children} = props
 return(
    <button onClick={onClick} type={type} className={classNames}>{children}</button>
 )
}

Button.propTypes = {
   onClick: PropTypes.func,
   type: PropTypes.string,
   classNames: PropTypes.string,
   children: PropTypes.any
}

export default Button