import React from "react";

import PropTypes from 'prop-types'
import ModalWrapper from "../Modal/ModalWrapper"
import Modal from "../Modal/Modal"
import ModalHeader from "../Modal/ModalHeader"
import ModalBody from "../Modal/ModalBody"
import ModalFooter from "../Modal/ModalFooter"
import ModalClose from "../Modal/ModalClose"

import "./ModalImage.scss"

const ModalImage = ({title,img, desc, handleOk, closeModal,handleBtn,isOpen }) =>{
    return(
        <ModalWrapper isOpen={isOpen}>
            <Modal classNames="modal modalImage">
                <ModalHeader>
                <ModalClose onclick={closeModal}/> 
                </ModalHeader>
                <ModalBody>
                    <img className="img-modal" src={img} alt="" />
                    <p className="delete-message">{title}</p>
                    <p className="instructie">{desc}</p>
                </ModalBody>
                <ModalFooter classNamesFirst="firstbutton" classNamesSecond="firstbutton second" firstText="NO, CANCEL" secondaryText="YES, ADD" firstClick={handleBtn} secondaryClick={handleOk}/>
            </Modal>
        </ModalWrapper>
    )
}
ModalImage.propTypes = {
    closeModal: PropTypes.func,
    img: PropTypes.string,
    title: PropTypes.string,
    dsc: PropTypes.string,
    handleOk: PropTypes.func,
    handleBtn: PropTypes.func,
    isOpen: PropTypes.func
}

export default ModalImage
