import "./Input.scss"
import React from "react";
import cn from "classnames"
import PropTypes from 'prop-types'
import {Field, ErrorMessage} from "formik"

function InputBox(props){
    const {
className,
label,
type,
name,
placeholder,
error,
classNameadr,
...restProps
    } = props
return(
    <label className={cn('form-item', className, {'has-validation': error})}>
     <p className="form-label">{label}</p>
    <Field type={type} className={`form-control ${classNameadr}` }name={name}  placeholder={placeholder} {...restProps}/>
    <ErrorMessage className="error-message" name={name} component="p"/>
    </label>
)
}
InputBox.defaultProps = {
	type: "text"
}

InputBox.propTypes = {
	className: PropTypes.string,
	label: PropTypes.string.isRequired,
	type: PropTypes.string,
	name: PropTypes.string.isRequired,
	placeholder: PropTypes.string,
	error: PropTypes.bool
}


export default InputBox