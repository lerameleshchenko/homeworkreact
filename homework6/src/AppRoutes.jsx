import "./App.css"
import Header from "./composition/Header/Header"
import Footer from "./composition/Footer/Footer"

import FvPage from "./pages/FvPage/FvPage.jsx"
import BuyPage from "./pages/BuyPage/BuyPage.jsx"
import HomePage from "./pages/HomePage/HomePage.jsx"
import NotPage from "./pages/NotPage/NotPage.jsx"
import MacPage from "./pages/MacPage/MacPage.jsx"
import IpadPage from "./pages/IpadPage/IpadPage.jsx"
import IphonePage from "./pages/IphonePage/IphonePage.jsx"
import WatchPage from "./pages/WatchPage/WatchPage.jsx"
import AirPodsPage from "./pages/AirPodsPage/AirPodsPage.jsx"
import FormPage from "./pages/FormPage/FormPage.jsx"
import ThanksPage from "./pages/ThanksPage/ThanksPage.jsx"
import {Route, Routes} from "react-router-dom"

function AppRoutes() { 

  return (
    <>
  <Header />
     <main> 
<Routes>
  <Route path="/" element={<HomePage/>}/>
  <Route path="Mac" element={<MacPage/>}/>
  <Route path="fvPage" element={<FvPage/>}/>
  <Route path="BuyPage" element={<BuyPage/>}/>
  <Route path="*" element={<NotPage/>}/>
  <Route path="iPad" element={<IpadPage/>}/>
  <Route path="iphone" element={<IphonePage/>}/>
  <Route path="watch" element={<WatchPage/>}/>
  <Route path="airPods" element={<AirPodsPage/>}/>
  <Route path="buy/ThanksPage" element={<ThanksPage/>}/>
   <Route path="BuyPage/buy" element={<FormPage/>}/>
</Routes>
 </main>
    <Footer/>
    </>
   
   ) 
}

export default AppRoutes