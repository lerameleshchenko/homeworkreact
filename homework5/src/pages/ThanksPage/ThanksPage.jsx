import { Link } from "react-router-dom"

import "./ThanksPage.scss"
function ThanksPage(){
    return(
        <div className="thanksContainer">
            <p className="thanksTekst">Дякуємо за покупку!</p>
            <Link className="linkHome" to="/">Home Page</Link>
          
        </div>
    )
}

export default ThanksPage