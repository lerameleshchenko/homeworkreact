import {Formik, Form} from "formik"
import {useDispatch, useSelector} from "react-redux";
import { useNavigate } from "react-router-dom";
import ModalText from "../../components/ModalText/ModalText";
import { validationSchema } from "./validation";


import { selectFromData } from "../../store/selectors";
import { actionUpDateForm , actionCleanCart, actionIsModals} from "../../store/actions";
import InputBox from "../../components/Form/Input/Input";
import Button from "../../components/Button/Button";
import { selectAddBuy ,selectIsModals} from "../../store/selectors";
import "./FormPAge.scss"


function FormPage(){

    const formData = useSelector(selectFromData)
    const buyArr = useSelector(selectAddBuy)
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const subtotal = buyArr.reduce((acc, { price, quantity }) => acc + price * quantity, 0);
   const isModal = useSelector(selectIsModals)
   function modalBool(){
    dispatch(actionIsModals())
   }

    function cleanArrBuy(){
      dispatch(actionCleanCart())
    }
    const handleYesClick = () => {
      cleanArrBuy();
      dispatch(actionUpDateForm(formData));
      navigate('/buy/ThanksPage')
      modalBool()
    };
    return(
      <div className="page__dashboard">
        <div className="page">
            <p className="checkOut">Check Out</p>
            <p className="billing">Billing Details</p>
         <Formik 
         initialValues={formData}
         validationSchema={validationSchema}
         onSubmit={(values, {resetForm})=>{
          
          const isAllFieldsFilled = Object.values(values).every(value => !!value.trim());
          if(isAllFieldsFilled){
            modalBool()
          }else{
            console.log("Not all fields are filled")
          }
          console.log("Продукти та інфо. про покупця", {values , buyArr });
         }}>
         {({errors,touched}) =>{
           return(
           <>
 <Form>
<div  className="InputAndProduct">
<div className="containerInput">
                    <InputBox
                    label="First Name*"
                    name="firstName"
                    placeholder="First Name"
                    error={errors.firstName && touched.firstName}
                
                    />
                     <InputBox
                    label="Last Name*"
                    name="lastName"
                    placeholder="Last Name"
                    error={errors.lastName && touched.lastName}
                  
                    />
                      <InputBox
                    label="Age*"
                    name="age"
                    placeholder="Age"
                    error={errors.age && touched.age}
                 
                    />
                      <InputBox
                    label="Country*"
                    name="country"
                    placeholder="Country"
                    error={errors.country && touched.country}
                   
                    />
                       <InputBox
                       classNameadr="adres"
                    label="Street Address*"
                    name="streetAddress"
                    placeholder="Street Address"
                    error={errors.streetAddress && touched.streetAddress}
                  
                    />
                       <InputBox
                         classNameadr="adres"
                    label="City*"
                    name="city"
                    placeholder="City"
                    error={errors.city && touched.city}
                    
                    />
                        <InputBox
                          classNameadr="adres"
                    label="Postal Code*"
                    name="postalCode"
                    placeholder="Postal Code"
                    error={errors.postalCode && touched.postalCode}
                   
                    />
                         <InputBox
                    label="Phone*"
                    name="phone"
                    placeholder="Phone"
                    error={errors.phone && touched.phone}
                   
                    />
                </div>
           
        <div className="containerProduct">
            <p className="order">Order Summary</p>
            <div className="cardItem">
        {buyArr.map(({img,name,price,quantity,color}, index) => {
            
            return (
                <div className="cardItemContainen" key={index}>
                    <img className="formImg" src={img} alt={name} />
                    <div className="nameProductBuy">
                         <p className="name" >{name} </p>
                          <p className="colorProduct">Color : {color}</p>
                    </div>
                    <p className="quantityProductBuy">x {quantity}</p>
                    <p className="priceProduct">€{price * quantity}</p>
                    
                </div>
            );
        })}
    </div>
       <p className="total">Subtotal ({buyArr.length}) €{subtotal} </p>
       <p className="total total2">Total €{subtotal}</p>
        </div>
              </div>
     <div className="btnBuy">
        <Button classNames="submitBuy" type="submit">Continue to delivery</Button> 
       </div>
            </Form> 
           
           </>   
           )
        }}
         </Formik>
        </div>
        {isModal &&
         <ModalText
         title='confirm purchase'
         desc="make sure everything is correct"
         isOpen={modalBool}
         closeModal={modalBool}
         firstText="buy"
         handleBtn={() => handleYesClick()}
       />
        }
       
      </div>
     
    )
}

export default FormPage