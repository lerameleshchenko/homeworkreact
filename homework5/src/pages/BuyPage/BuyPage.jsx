import ProductCard from "../../components/Product/ProductCard";
import ProductItem from "../../components/Product/ProductItem";
import ModalText from "../../components/ModalText/ModalText";


import "./BuyPage.scss"
import { useState } from "react";
import { Link } from "react-router-dom";

import {useDispatch, useSelector} from "react-redux"
import { actionIsModals, actionProductInBuyDelete} from "../../store/actions";
import { selectIsModals, selectAddBuy} from "../../store/selectors";

function BuyPage(){ 
  const dispatch = useDispatch();
  const buyArr = useSelector(selectAddBuy);
  const deleteModalCart = useSelector(selectIsModals);


  
  const [selectedProduct, setSelectedProduct] = useState(null);
 

  function deleteCardFromLocal(articleToRemove) {
    const currentCart = JSON.parse(localStorage.getItem('cart')) || [];
    const updatedCart = currentCart.filter((product) => product.article !== articleToRemove);
    localStorage.setItem('cart', JSON.stringify(updatedCart));
  }



  function deleteModal() {
    dispatch(actionIsModals());
  }

  const add = (index) => {
    deleteModal();
    setSelectedProduct(buyArr[index]);
  };

 

  const handleDeleteFromCart = (article) => {
      dispatch(actionProductInBuyDelete(article));
    deleteCardFromLocal(article);
    add()
  };
 
  return (
    <>
      <ProductCard>
        {buyArr && buyArr.length > 0 ? (
          buyArr.map(({ name, article, color, img,price, quantity }, index) => (
            <ProductItem
              key={index}
              name={name}
              color={color}
              price={price * quantity}
              article={article}
              img={img}
              closeIcon="iconClose"
              clickIconDelete={() => add(index)}
              addcardText={`(${quantity})`}
             
            />
          ))
        ) : (
          <p>Немає доданих товарів</p>
        )}
        
      </ProductCard>
      {buyArr.length > 0 && <div className="goBuy"><Link to="buy" className="buy"> {`Add to cart (${buyArr.length })`} </Link></div>}
      {deleteModalCart && selectedProduct && (
        <ModalText
          title={`Delete ${selectedProduct.name}?`}
          desc="Description for your product"
          isOpen={add}
          closeModal={add}
          firstText="Delete?"
          handleBtn={() => handleDeleteFromCart(selectedProduct.article)}
        />
      )}
      
    </>
  );
}

export default BuyPage