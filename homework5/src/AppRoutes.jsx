import "./App.css"
import Header from "./composition/Header/Header"
import Footer from "./composition/Footer/Footer"

import FvPage from "./pages/FvPage/FvPage.jsx"
import BuyPage from "./pages/BuyPage/BuyPage.jsx"
import HomePage from "./pages/HomePage/HomePage.jsx"
import NotPage from "./pages/NotPage/NotPage.jsx"
import MacPage from "./pages/MacPage/MacPage.jsx"
import FormPage from "./pages/FormPage/FormPage.jsx"
import ThanksPage from "./pages/ThanksPage/ThanksPage.jsx"


import {Route, Routes} from "react-router-dom"
import { useSelector} from "react-redux"
import { selectAddBuy,selectAddFav} from "./store/selectors.js"




function AppRoutes() { 
  const cart = useSelector(selectAddBuy)
const favoriteProducts = useSelector(selectAddFav)


  return (
    <>
  <Header num1={favoriteProducts.length} num2={cart.length}/>
     <main> 
<Routes>
  <Route path="/" element={<HomePage/>}/>
  <Route path="fvPage" element={<FvPage/>}/>
  <Route path="BuyPage" element={<BuyPage/>}/>
  <Route path="*" element={<NotPage/>}/>
  <Route path="Mac" element={<MacPage/>}/>
  <Route path="BuyPage/buy" element={<FormPage/>}/>
  <Route path="buy/ThanksPage" element={<ThanksPage/>}/>
</Routes>
 </main>
    <Footer/>
    </>
   
   ) 
}

export default AppRoutes