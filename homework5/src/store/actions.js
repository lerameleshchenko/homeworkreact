import { createAction } from "@reduxjs/toolkit"
import { sendRequest } from "../helpers/sendRequest.js"


export const actionAddAllProducts = createAction("ACTION_ADD_ALL_PRODUCTS")

export const actionFetchAllProducts = () => (dispatch) =>{
    return sendRequest('/public/data.json')
    .then((data) =>{
        dispatch(actionAddAllProducts(data))
    })
}

export const actionIsModals = createAction("ACTION_IS_MODAL")
export const actionProductInBuy = createAction("ACTION_PRODUCT_IN_BUY")
export const actionProductInBuyDelete = createAction("ACTION_PRODUCT_IN_BUY_DELETE")
export const actionProductInFav = createAction("ACTION_PRODUCT_IN_FAV")
export const actionProductInFavDelete = createAction("ACTION_PRODUCT_IN_FAV_DELETE")
export const setIsFavorite = createAction('SET_IS_FAVORITE')
export const actionUpDateForm = createAction('ACTION_UP_DATE_FORM')
export const actionCleanCart = createAction('ACTION_CLEAN_CART')






