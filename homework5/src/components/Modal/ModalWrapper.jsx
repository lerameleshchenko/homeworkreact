import PropTypes from 'prop-types'

import "./Modal.scss"

function ModalWrapper({children, isOpen}){
    return(
 
    <div onClick={isOpen} className="modal-wraper"> {children}</div> 
 
    )
}

ModalWrapper.propTypes = {
    children: PropTypes.any,
    isOpen: PropTypes.func
}

export default ModalWrapper