function Modal({ children, classNames }) {
    const stopPropagation = (e) => {
        e.stopPropagation();
    };

    return (
        <div className={classNames} onClick={stopPropagation}>
            <div className="box-modal">
                {children}
            </div>
        </div>
    );
}

export default Modal;