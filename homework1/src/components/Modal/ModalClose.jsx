function ModalClose({onclick}){
    return(
        <button type="button" onClick={onclick} className="icon">
 <svg  width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg"> 
                <g id="close cross"> 
                <path id="Icon" d="M18 4L4 18M18 18L4 4.00001" stroke="#3C4242" strokeWidth="1.5" strokeLinecap="round"/> 
                </g> 
                </svg>  
        </button>
    )
}

export default ModalClose