import "./Modal.scss"

function ModalWrapper({children, isOpen}){
    return(
 
    <div onClick={isOpen} className="modal-wraper"> {children}</div> 
 
    )
}



export default ModalWrapper