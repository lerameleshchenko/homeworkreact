import ModalWrapper from "./ModalWrapper";
import Modal from "./Modal";
import ModalHeader from "./ModalHeader";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";


function ModalBase({closeModal, img, title,dsc, handleOk,handleBtn,isOpen}){
    return(
        <ModalWrapper isOpen={isOpen}>
            <Modal>
            <ModalHeader>
            <ModalClose onclick={closeModal}/>
            </ModalHeader>
            <ModalBody>
            <div className={img}></div>
            <p>{title}</p>
            <p>{dsc}</p>
            </ModalBody>
            <ModalFooter classNamesFirst="firstbutton" classNamesSecond="firstbutton second" firstText="NO, CANCEL" secondaryText="YES, DELETE" firstClick={handleBtn} secondaryClick={handleOk}></ModalFooter>
            </Modal>
            
        </ModalWrapper>
    )
}

export default ModalBase