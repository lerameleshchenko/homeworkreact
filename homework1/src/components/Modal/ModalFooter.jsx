import Button from "../Button/Button"

function ModalFooter({firstText, secondaryText, firstClick, secondaryClick, classNamesFirst, classNamesSecond}){
    return(
        <>
        <div className="buttons">
        {firstText && <Button classNames={classNamesFirst} onClick={firstClick}>{firstText}</Button>}
        {secondaryText && <Button classNames={classNamesSecond} onClick={secondaryClick}>{secondaryText}</Button>}
        </div>

        </>
    
    )
}


export default ModalFooter