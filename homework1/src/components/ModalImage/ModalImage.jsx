import ModalWrapper from "../Modal/ModalWrapper"
import Modal from "../Modal/Modal"
import ModalHeader from "../Modal/ModalHeader"
import ModalBody from "../Modal/ModalBody"
import ModalFooter from "../Modal/ModalFooter"
import ModalClose from "../Modal/ModalClose"

const ModalImage = ({title,img, desc, handleOk, closeModal,handleBtn,isOpen }) =>{
    return(
        <ModalWrapper isOpen={isOpen}>
            <Modal classNames="modal modalImage">
                <ModalHeader>
                <ModalClose onclick={closeModal}/> 
                </ModalHeader>
                <ModalBody>
                    <div className={img}></div>
                    <p className="delete-message">{title}</p>
                    <p className="instructie">{desc}</p>
                </ModalBody>
                <ModalFooter classNamesFirst="firstbutton" classNamesSecond="firstbutton second" firstText="NO, CANCEL" secondaryText="YES, DELETE" firstClick={handleBtn} secondaryClick={handleOk}/>
            </Modal>
        </ModalWrapper>
    )
}






export default ModalImage
