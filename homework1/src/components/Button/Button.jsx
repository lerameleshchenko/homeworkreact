import './button.scss';

function Button(props){
    const{type="button", classNames = "", onClick, children} = props
 return(
    <button onClick={onClick} type={type} className={classNames}>{children}</button>
 )
}


export default Button