import "./App.css"
import React, { useState } from 'react';
import Button from './components/Button/Button'
import ModalImage from "./components/ModalImage/ModalImage";
import ModalText from "./components/ModalText/ModalText";

function App() {
 
  const [buttonFirst, setButtonFirst] = useState(false);

  const openModal = () =>{
    setButtonFirst(!buttonFirst)
  }
 
  const [buttonSecond, setButtonSecond] = useState(false);

  const openModalSecond = () =>{
    setButtonSecond(!buttonSecond)
  }
  
  return (
    <>
    <div className='container-buttons'>
       <Button onClick={openModal} classNames={"button"} >Open first modal</Button>
   <Button onClick={openModalSecond} classNames={"button"} >Open second modal</Button>
   </div>
   
   {buttonFirst &&(
   <ModalImage
      isOpen={openModal}
      title="Product Delete!"
      desc="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."
      img="image"
      closeModal={openModal}  
      handleBtn={openModal}
      handleOk={openModal}
   />
   )}
      
  {buttonSecond && (
    <ModalText
    isOpen={openModalSecond}
   closeModal={openModalSecond}
   title="Add Product “NAME”"
   desc="Description for you product"
   handleBtn={openModalSecond}
   />  

  )}
  </>
  )
}

export default App
