import ProductCard from "../../components/Product/ProductCard";
import ProductItem from "../../components/Product/ProductItem";
import ModalText from "../../components/ModalText/ModalText";
import "./BuyPage.scss"
import { useState } from "react";
function BuyPage({deleteCard}){ 
  const productBuy = localStorage.getItem('cart');
  const [buyArr, setBuyArr] = useState(JSON.parse(productBuy) || []);

  const handleDeleteFromCart = (article) => {
    const updatedCart = buyArr.filter((product) => product.article !== article);
    localStorage.setItem('cart', JSON.stringify(updatedCart));
    setBuyArr(updatedCart);
   
    deleteCard(article)
    add()
  }

  const [deleteCardInBuy, setdeleteCardInBuy] = useState(false)

  const [selectedProduct, setSelectedProduct] = useState(null);


  function add(index){
    setdeleteCardInBuy(!deleteCardInBuy)
    setSelectedProduct(buyArr[index]);
  }
    return(
    <>
    
            <ProductCard>
        {buyArr && buyArr.length > 0 ? (
        buyArr.map(({ name, article, color, img, price }, index) => ( 
          <ProductItem 
            key={index}
            name={name}
            color={color}
            price={price}
            article={article}
            img={img}
          closeIcon="iconClose"
       clickIconDelete={()=> add(index)}
          />
        ))
      ) : (
        <p>Немає доданих товарів</p>
      )} </ProductCard>
   {deleteCardInBuy && selectedProduct && <ModalText
   title={`Delete ${selectedProduct.name}?`}
   desc="Description for you product"
   isOpen={add}
   closeModal={add}
   handleBtn={()=> handleDeleteFromCart(selectedProduct.article)}
   />}
    
    
    </>
   
    
    )
}

export default BuyPage