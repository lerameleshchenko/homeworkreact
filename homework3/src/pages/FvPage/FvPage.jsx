import "./FvPage.scss"
import ProductCard from "../../components/Product/ProductCard";
import ProductItem from "../../components/Product/ProductItem";

import { useState } from "react";

function FvPage({ updateMainPage }) {
  const productFv = localStorage.getItem('productInFavorite');
  const [favoriteArr, setFavoriteArr] = useState(JSON.parse(productFv) || []);

  const handleDeleteFavorite = (article) => {
    const updatedFavorites = favoriteArr.filter((product) => product.article !== article);
    localStorage.setItem('productInFavorite', JSON.stringify(updatedFavorites));
    setFavoriteArr(updatedFavorites);


    updateMainPage(updatedFavorites);
  };


    return(
           <ProductCard >
         {favoriteArr && favoriteArr.length > 0 ? (
        favoriteArr.map(({ name, article, color, img, price }, index) => (
          <ProductItem
            key={index}
            name={name}
            color={color}
            price={price}
            article={article}
            img={img}
            classNameFav="fv favorite" 
            clickFavorite={() => handleDeleteFavorite(article)}
          />
        ))
      ) : (
        <p>Немає доданих товарів</p>
      )}
       
          </ProductCard>  
     

      
   
    )
}

export default FvPage