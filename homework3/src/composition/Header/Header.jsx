import Logo from "../icons/logo.svg?react"
import Favorite from "../icons/favorite.svg?react"
import Buy from "../icons/bay.svg?react"
import PropTypes from "prop-types"
import "./Header.scss"
import {Link} from "react-router-dom"
function Header({num1,num2}){
    return(
        <>
          <header className="header">
           <Link to="/"><Logo/></Link> 
            <div className="container-for-svg">
           <Link to="fvPage"><Favorite/></Link> 
            <p className="numF">{num1}</p>
            <Link to="BuyPage"><Buy/></Link>
            <p className="numB">{num2}</p>
            </div>
           
        </header>
       
        </>
      
    )
}

// Header.propTypes = {
//     num1: PropTypes.number,
//     num2: PropTypes.number
// }

export default Header