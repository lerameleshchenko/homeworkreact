import "./App.css"
import Header from "./composition/Header/Header"
import Footer from "./composition/Footer/Footer"


import ModalImage from "./components/ModalImage/ModalImage.jsx"
import ProductCard from "./components/Product/ProductCard.jsx"
import ProductItem from "./components/Product/ProductItem.jsx"
import { sendRequest } from "./helpers/sendRequest.js"
import {Route, Routes} from "react-router-dom"
import {useState, useEffect} from "react"
import FvPage from "./pages/FvPage/FvPage.jsx"
import BuyPage from "./pages/BuyPage/BuyPage.jsx"




function App() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    sendRequest('/public/data.json')
      .then((data) => {
          setProducts(data)
      })
      
  }, []);

 const [isFavorite, setIsFavorite] = useState(Array(products.length).fill(false));




let favoriteProducts = JSON.parse(localStorage.getItem('productInFavorite')) || [];
  function changeFavorite(index) { // index кожної карточки
    const newIsFavorite = [...isFavorite];
    newIsFavorite[index] = !newIsFavorite[index]; //  булове значення 
  
    setIsFavorite(newIsFavorite);
  
    localStorage.setItem('favorites', JSON.stringify(newIsFavorite));
    
    if(!isFavorite[index]){
        favoriteProducts.push(products[index]);
    }else{
      const productIndex = favoriteProducts.findIndex(product => product.article === products[index].article);
      if (productIndex !== -1) {
        favoriteProducts.splice(productIndex, 1);
      }
    }

    localStorage.setItem('productInFavorite', JSON.stringify(favoriteProducts));

  }





//// ADD CARD


  const [addCard, setAddCard ] = useState(false)

  const [selectedProduct, setSelectedProduct] = useState(null);

    const [cart, setCart] = useState([]);

  function add(index){
    setAddCard(!addCard)
    setSelectedProduct(products[index]);
  }

  function addPlusNum() {
    const updatedCart = [...cart, selectedProduct];
    setCart(updatedCart);

    localStorage.setItem('cart', JSON.stringify(updatedCart));
    add();
 
  }

  function deleteCard(articleToRemove){
    const updatedCart = cart.filter((product) => product.article !== articleToRemove);
    setCart(updatedCart);
    localStorage.setItem('cart', JSON.stringify(updatedCart));
  }
// localStorage



useEffect(() => {
  const storedCart = JSON.parse(localStorage.getItem('cart'));
  if (storedCart) {
    setCart(storedCart);
  }
}, []);

useEffect(() => {
  const storedFavorites = JSON.parse(localStorage.getItem('favorites'));
  if (storedFavorites) {
setIsFavorite(storedFavorites);
  }
 
}, []);

const updateMainPage = (updatedFavorites) => {
  const updatedIsFavorite = Array(products.length).fill(false);

  updatedFavorites.forEach((favProduct) => {
    const index = products.findIndex((product) => product.article === favProduct.article);
    if (index !== -1) {
      updatedIsFavorite[index] = true;
    }
  });

  setIsFavorite(updatedIsFavorite);
};



  return (
    <>
  <Header num1={favoriteProducts.length} num2={cart.length}/>
     <main>
<Routes>
  <Route path="/" element={   
     <ProductCard> 
        < div className="img"></div>
        {products.map((product, index) => (   
                <ProductItem 
                key={index}
                name={product.name}
                color={product.color}
                price={product.price}
                article={product.article}
                img={product.img}
                clickFavorite={()=>changeFavorite(index)}
                classNameFav={`fv ${isFavorite[index] ? "favorite" : ""}`}
                addToCard={() => add(index)}
                />             
              ))}
          </ProductCard>
        }/>


  <Route path="fvPage" element={<FvPage updateMainPage={updateMainPage}/>}/>
  <Route path="BuyPage" element={<BuyPage deleteCard={deleteCard} />}/>
</Routes>
 </main>
 
 {addCard && selectedProduct &&(
          <ModalImage
          img={selectedProduct.img}
          title={`Add Product ${selectedProduct.name}?`}
          desc="Description for you product"
          closeModal={add}
          handleBtn={add}
          handleOk={addPlusNum}
          isOpen={add}
        />
        )}
    
    <Footer/>
    </>
   
   ) 
}

export default App
 