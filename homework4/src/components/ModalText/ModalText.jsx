import ModalWrapper from "../Modal/ModalWrapper"
import Modal from "../Modal/Modal"
import ModalHeader from "../Modal/ModalHeader"
import ModalBody from "../Modal/ModalBody"
import ModalFooter from "../Modal/ModalFooter"
import ModalClose from "../Modal/ModalClose"


function ModalText({title, desc, closeModal,handleBtn,isOpen}){
    return(
        <ModalWrapper isOpen= {isOpen}>
            <Modal classNames="modal">
                <ModalHeader>
                <ModalClose onclick={closeModal}/> 
                </ModalHeader>
                <ModalBody>
                    <p className="delete-message">{title}</p>
                    <p className="instructie">{desc}</p>
                </ModalBody>
                <ModalFooter classNamesFirst="firstbutton" firstText="Delete?"  firstClick={handleBtn}/>
            </Modal>
        </ModalWrapper>
    )
}


export default ModalText