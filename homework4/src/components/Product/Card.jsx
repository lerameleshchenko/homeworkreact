import ProductCard from "./ProductCard";
import ProductItem from "./ProductItem";
import ModalImage from "../ModalImage/ModalImage";
import "./Product.scss"
import {useState, useEffect} from "react"

import {useDispatch, useSelector} from "react-redux"
import {actionFetchAllProducts,actionIsModals, actionProductInFav, actionProductInFavDelete,actionProductInBuy} from "../../store/actions";
import { selectIsModals,selectAllProducts,selectAddFav,selectAddBuy } from "../../store/selectors";
function Card(){
    //add fav
    const addedToCart = useSelector(selectAddBuy);
    const products = useSelector(selectAllProducts)
    const dispatch = useDispatch()
   const [isFavorite, setIsFavorite] = useState(Array(products.length).fill(false));
  const favoriteProducts = useSelector(selectAddFav)
  
    useEffect(() => {
      dispatch(actionFetchAllProducts())
     
        
    }, []);
  
  useEffect(() => {
    const newIsFavorite = Array(products.length).fill(false);
    favoriteProducts.forEach((favProduct) => {
      const index = products.findIndex((product) => product.article === favProduct.article);
      if (index !== -1) {
        newIsFavorite[index] = true;
      }
    });
    setIsFavorite(newIsFavorite);
  }, [favoriteProducts, products]);
  

    function changeFavorite(index) { // index кожної карточки
        const newIsFavorite = [...isFavorite];
        newIsFavorite[index] = !newIsFavorite[index];
        setIsFavorite(newIsFavorite);
    
        const productFav = products[index];
        if (newIsFavorite[index]) {
          dispatch(actionProductInFav(productFav));
          const currentCartFaxv = JSON.parse(localStorage.getItem('favoriteProducts')) || [];
          currentCartFaxv.push(productFav);
          localStorage.setItem('favoriteProducts', JSON.stringify(currentCartFaxv));
        } else {
          dispatch(actionProductInFavDelete(productFav.article));
          const currentCartDelFav = JSON.parse(localStorage.getItem('favoriteProducts')) || [];
          const updatedCartfav = currentCartDelFav.filter((product) => product.article !== productFav.article);
          localStorage.setItem('favoriteProducts', JSON.stringify(updatedCartfav));
        }
        }

//addBuy
        const [selectedProduct, setSelectedProduct] = useState(null);
        const addCard = useSelector(selectIsModals)
    
        
         function hendlModal(){
            dispatch(actionIsModals())
         }
        
        function add(index){
         hendlModal()
         setSelectedProduct(products[index]);
        }
        
        function addPlusNum() {
        dispatch(actionProductInBuy(selectedProduct))
        
        const currentCart = JSON.parse(localStorage.getItem('cart')) || [];
        currentCart.push(selectedProduct);
        localStorage.setItem('cart', JSON.stringify(currentCart));
         add();
        }
    return(
        
        <>
        < div className="img"></div>
                <ProductCard> 
        {products.map((product, index) => (   
                <ProductItem 
                key={index}
                name={product.name}
                color={product.color}
                price={product.price}
                article={product.article}
                img={product.img}
                clickFavorite={()=>changeFavorite(index)}
                classNameFav={` ${isFavorite[index] ? "favorite" : ""}`}
                addToCard={() => add(index)}
                addcardText={` ${addedToCart.some((item) => item.article === product.article) ? '✔added' : ' add card'}`}
                />             
              ))}
          </ProductCard>
           
 {addCard && selectedProduct &&(
          <ModalImage
          img={selectedProduct.img}
          title={`Add Product ${selectedProduct.name}?`}
          desc="Description for you product"
          closeModal={add}
          handleBtn={add}
          handleOk={addPlusNum}
          isOpen={add}
        />
        )}
        </>   
    )
}

export default Card