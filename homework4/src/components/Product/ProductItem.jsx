import FavoriteTwo from "../../composition/icons/favorite2.svg?react"
import CloseIcon from "../../composition/icons/icons8-close.svg?react"
import Button from "../Button/Button"

import PropTypes from 'prop-types'

import "./Product.scss"

function ProductItem({ name, price, img, article, color,clickFavorite,classNameFav,addcardText, addToCard, closeIcon,clickIconDelete}){
    return(
        <li className="product-card">
        <img className="all-img" src={img} alt={name} />
        <div className="name">
        <h3 className="nameProduct">{name}</h3>
        <p className="price">€ {price}</p>
        <p> {color}</p>
        </div>
       <div className="add">
       <Button onClick={addToCard} classNames="btn-add"><p className="textAddCard">{addcardText}</p></Button>
       {closeIcon && (<Button classNames="btn-add" className={closeIcon} onClick={clickIconDelete}>delete</Button>)}
       {classNameFav &&( <FavoriteTwo onClick={clickFavorite} className={classNameFav}/> )}
       </div>
         </li>
    )
}


ProductItem.defaultProps = {
    clickFavorite: () => {},
    addToCard: ()=>{}
  }

ProductItem.propTypes = {
    name: PropTypes.string,
    img: PropTypes.string,
    price: PropTypes.number,
    article: PropTypes.number,
    color: PropTypes.string,
    classNameFav: PropTypes.any,
    clickFavorite: PropTypes.func,
    addToCard: PropTypes.func
}


export default ProductItem