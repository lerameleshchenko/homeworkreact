import "./App.css"
import Header from "./composition/Header/Header"
import Footer from "./composition/Footer/Footer"

import FvPage from "./pages/FvPage/FvPage.jsx"
import BuyPage from "./pages/BuyPage/BuyPage.jsx"
import Card from "./components/Product/Card.jsx"

import {Route, Routes} from "react-router-dom"
import { useSelector} from "react-redux"
import { selectAddBuy,selectAddFav} from "./store/selectors.js"




function AppRoutes() { 
  const cart = useSelector(selectAddBuy)
const favoriteProducts = useSelector(selectAddFav)


  return (
    <>
  <Header num1={favoriteProducts.length} num2={cart.length}/>
     <main> 
<Routes>
  <Route path="/" element={<Card/>}/>
  <Route path="fvPage" element={<FvPage/>}/>
  <Route path="BuyPage" element={<BuyPage/>}/>
</Routes>
 </main>
    <Footer/>
    </>
   
   ) 
}

export default AppRoutes