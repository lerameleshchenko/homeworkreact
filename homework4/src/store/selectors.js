export const selectAllProducts = (state) => state.allProducts
export const selectIsModals = (state) => state.isModal
export const selectAddBuy = (state) => state.buyArr
export const selectAddFav = (state) => state.favArr
export const selectIsFavorite = (state) => state.isFavorite