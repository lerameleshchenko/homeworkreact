import {configureStore} from '@reduxjs/toolkit'

import rootReducer from './reducers.js'

export default configureStore({
    reducer:rootReducer
})