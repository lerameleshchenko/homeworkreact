import {createReducer} from "@reduxjs/toolkit"

import * as actions from "./actions.js"

const initialState = {
allProducts:[],
isModal:false,
buyArr: JSON.parse(localStorage.getItem('cart')) || [],
favArr: JSON.parse(localStorage.getItem('favoriteProducts')) || [],

}

export default createReducer(initialState,{
    [actions.actionAddAllProducts]: (state, {payload})=>{
        state.allProducts =  payload
    },
    [actions.actionIsModals]: (state)=>{
        state.isModal = !state.isModal
    },
   [actions.actionProductInBuy]: (state, {payload})=>{
    state.buyArr = [...state.buyArr, payload]
   },
   [actions.actionProductInBuyDelete]: (state, {payload}) =>{
    state.buyArr = state.buyArr.filter((product) => product.article !== payload);
   },
   [actions.actionProductInFav]: (state, {payload})=>{
    state.favArr = [...state.favArr, payload]
   },
   [actions.actionProductInFavDelete]: (state, { payload }) => {
    state.favArr = state.favArr.filter((product) => product.article !== payload);
  }
})
