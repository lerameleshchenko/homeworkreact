import ProductCard from "../../components/Product/ProductCard";
import ProductItem from "../../components/Product/ProductItem";
import ModalText from "../../components/ModalText/ModalText";
import "./BuyPage.scss"
import { useState } from "react";

import {useDispatch, useSelector} from "react-redux"
import { actionIsModals, actionProductInBuyDelete} from "../../store/actions";
import { selectIsModals, selectAddBuy} from "../../store/selectors";

function BuyPage(){ 
  const dispatch = useDispatch()
 const buyArr = useSelector(selectAddBuy)

function deleteCardFromLocal(articleToRemove){
   const currentCart = JSON.parse(localStorage.getItem('cart')) || [];
   const updatedCart = currentCart.filter((product) => product.article !== articleToRemove);
   localStorage.setItem('cart', JSON.stringify(updatedCart));
  }
  
console.log("buy",buyArr)
  const handleDeleteFromCart = (article) => {
   dispatch(actionProductInBuyDelete(article))
    
   deleteCardFromLocal(article)
    add()
  }

  const deleteModalCart = useSelector(selectIsModals)

  function deleteModal(){
    dispatch(actionIsModals())
 }
  const [selectedProduct, setSelectedProduct] = useState(null);


  function add(index){
    deleteModal()
    setSelectedProduct(buyArr[index]);
  }
  
    return(
    <>
    
            <ProductCard>
        {buyArr && buyArr.length > 0 ? (
        buyArr.map(({ name, article, color, img, price }, index) => ( 
          <ProductItem 
            key={index}
            name={name}
            color={color}
            price={price}
            article={article}
            img={img}
          closeIcon="iconClose"
       clickIconDelete={()=> add(index)}
       addcardText="buy"
          />
        ))
      ) : (
        <p>Немає доданих товарів</p>
      )} </ProductCard>
   {deleteModalCart && selectedProduct && <ModalText
   title={`Delete ${selectedProduct.name}?`}
   desc="Description for you product"
   isOpen={add}
   closeModal={add}
   handleBtn={()=> handleDeleteFromCart(selectedProduct.article)}
   />}
    
    
    </>
   
    
    )
}

export default BuyPage