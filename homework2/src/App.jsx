import "./App.css"
import Header from "./composition/Header/Header"
import Footer from "./composition/Footer/Footer"

import ProductItem from "./components/Product/ProductItem"
import ProductCard from "./components/Product/ProductCard"
import { useState, useEffect } from "react";
import {sendRequest} from "./helpers/sendRequest.js"
import ModalImage from "./components/ModalImage/ModalImage.jsx"



function App() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    sendRequest('/public/data.json')
      .then((data) => {
          setProducts(data)
      })
      
  }, []);


  const [isFavorite, setIsFavorite] = useState(Array(products.length).fill(false));

  function changeFavorite(index) { // index кожної карточки
    isFavorite[index] = !isFavorite[index];// булове значення 
    setIsFavorite(isFavorite);

    localStorage.setItem('favorites', JSON.stringify(isFavorite));

    if(isFavorite[index]){
      plusNum()
    }else{
     
      minNum()
    }
  }



  const [numFavorite, setNumFavorite]=useState(()=>{
    return Number(localStorage.getItem('numFavorite'))
  })
   

  function plusNum(){
    setNumFavorite(numFavorite + 1)
  }

  function minNum(){
    setNumFavorite(numFavorite - 1)
  }

//// ADD CARD


  const [addCard, setAddCard ] = useState(false)

  const [selectedProduct, setSelectedProduct] = useState(null);
  

  const [numBuy, setNumBuy]=useState(() => {
    return Number(localStorage.getItem('numBuy'));
  });

  function add(index){
    setAddCard(!addCard)
    setSelectedProduct(products[index]);
  }

  function plusNum2(){
    setNumBuy(numBuy + 1)
  }


  function addPlusNum() {
    plusNum2();
    add();
    
  }

// localStorage


useEffect(() => {
  const storedFavorites = JSON.parse(localStorage.getItem('favorites'));
  if (storedFavorites) {
setIsFavorite(storedFavorites);
  }
}, []);


  useEffect(() => {
    localStorage.setItem('numFavorite', numFavorite);
    localStorage.setItem('numBuy', numBuy);
  }, [numFavorite, numBuy]);

  return (
    <>
     <Header
     num1={numFavorite}
     num2={numBuy}
     />
     <main> 
      <ProductCard>
      {products.map((product, index) => (   
              <ProductItem
                key={index}
                name={product.name}
                color={product.color}
                price={product.price}
                article={product.article}
                img={product.img}
                clickFavorite={()=>changeFavorite(index)}
                classNameFav={`fv ${isFavorite[index] ? "favorite" : ""}`}
                addToCard={() => add(index)}
                classNameBuy="btn-add"
              />
            ))}
        </ProductCard>
        {addCard && selectedProduct &&(
          <ModalImage
          img={selectedProduct.img}
          title={`Add Product ${selectedProduct.name}`}
          desc="Description for you product"
          closeModal={add}
          handleBtn={add}
          handleOk={addPlusNum}
          isOpen={add}
        />
        )}
        
        </main>
    <Footer/>
    </>
   
   ) 
}

export default App
