import PropTypes from 'prop-types'

import Button from "../Button/Button"

function ModalFooter({firstText, secondaryText, firstClick, secondaryClick, classNamesFirst, classNamesSecond}){
    return(
        <>
        <div className="buttons">
        {firstText && <Button classNames={classNamesFirst} onClick={firstClick}>{firstText}</Button>}
        {secondaryText && <Button classNames={classNamesSecond} onClick={secondaryClick}>{secondaryText}</Button>}
        </div>
        </>
    
    )
}

ModalFooter.propTypes = {
    firstText: PropTypes.string,
    secondaryText: PropTypes.string,
    firstClick: PropTypes.func,
    secondaryClick: PropTypes.func,
    classNamesFirst: PropTypes.string,
    classNamesSecond: PropTypes.string
}


export default ModalFooter