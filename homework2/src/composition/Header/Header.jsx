import Logo from "../icons/logo.svg?react"
import Favorite from "../icons/favorite.svg?react"
import Buy from "../icons/bay.svg?react"
import PropTypes from "prop-types"
import "./Header.scss"

function Header({num1,num2}){
    return(
        <>
          <header className="header">
            <Logo/>
            <div className="container-for-svg">
            <Favorite/>
            <p className="numF">{num1}</p>
            <Buy/>
            <p className="numB">{num2}</p>
            </div>
           
        </header>
        <div className="img"></div>
        </>
      
    )
}

Header.propTypes = {
    num1: PropTypes.number,
    num2: PropTypes.number
}

export default Header